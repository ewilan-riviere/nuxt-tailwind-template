const app = 'nuxt/tailwind'
const author = 'NuxtJS'
const tailwind = require('../../tailwind.config')
const color = tailwind.theme.extend.colors.primary[600]
const description = 'To have Nuxt project with Tailwind JIT and HMR'

module.exports = {
  settings: {
    robots: 'index, follow',
    disallow: '/sign-in,/sign-up,/dashboard,/admin,/profile',
    color,
    locale: 'en_US',
    lang: 'en',
    googleToken: process.env.GOOGLE_SITE_VERIFICATION_TOKEN,
  },
  tags: {
    title: app,
    titleTemplate: `%s · ${app}`,
    description,
    rating: 'general',
    keywords: ['nuxt', 'tailwind', 'tech'],
    author,
    publisher: app,
    copyright: 'MIT License',
    language: 'english',
    designer: author,
  },
  og: {
    type: 'website',
    siteName: app,
  },
  twitter: {
    creator: '@nuxt_js',
    site: '@nuxt_js',
    url: `https://twitter.com/nuxt_js`,
  },
}
